import StatisticsBar from './stats';

const HeroSection = ({ title, address }) => {
  return (
    <div className="pb-12">
      <div className="flex flex-col py-20 justify-center text-white items-center">
        <div className="text-center text-5xl font-bold py-8">{title}</div>
        <div className="text-md text-center">{address}</div>
      </div>
      <StatisticsBar />
    </div>
  );
};
export default HeroSection;
