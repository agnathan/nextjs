const Stats = () => {
  let dealStats = [
    {
      value: '$18.3M',
      label: 'Purchase Price',
    },
    {
      value: '18.4%',
      label: 'Target Annual Return (IRR)',
    },
    {
      value: '2.2x',
      label: 'Target Equity Multiple',
    },
    {
      value: '5 Year',
      label: 'Target Hold Period',
    },
    {
      value: '13.2%',
      label: 'Target Cash On Cash',
    },
  ];

  var statsList = dealStats.map((stat) => {
    return (
      <dl className="flex flex-col text-center p-8">
        <div className="text-6xl">{stat.value}</div>
        <div className="text-lg">{stat.label}</div>
      </dl>
    );
  });

  console.log('dealStats:', statsList);

  return (
    <div className="flex flex-col lg:flex-row items-center text-white justify-center">
      {statsList}
    </div>
  );
};
export default Stats;
