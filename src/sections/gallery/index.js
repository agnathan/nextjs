const GallerySection = () => {
  const images = [
    { src: './assets/2.jpg' },
    { src: './assets/1.jpg' },
    { src: './assets/3.jpg' },
    { src: './assets/4.jpg' },
    { src: './assets/5.jpg' },
    { src: './assets/6.jpg' },
  ];

  const imagesList = images.map((img) => <img src={img.src} alt="none" />);
  return (
    <div class="text-center">
      <h1 class="text-4xl mt-12 tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl">
        <span class="block xl:inline">Propety</span>
        <span class="block text-indigo-600 xl:inline">Photos</span>
      </h1>
      <div class="grid grid-cols-3 gap-4 px-20 py-12">{imagesList}</div>
    </div>
  );
};

export default GallerySection;
