const FaqSection = () => {
  let items = [
    {
      question: 'When are distributions paid?',
      answer: 'Distributions of positive cash flow are paid quarterly.',
    },

    {
      question:
        'Do you have any past partners or professional references that I can contact?',
      answer:
        'We are happy to furnish you with as many references as you would like including past partners, lenders, brokers, and other professionals we have closed or worked with in the past. Please keep in mind, past performance is no indicator of future results and you should read the offering documents carefully.',
    },
    {
      question:
        'If I have questions throughout the term of my investment how can I get in touch with you?',
      answer:
        'As one of our partners, you have direct access to the principals of the firm and their investor relations support team. You can call or email us anytime.',
    },
    {
      question: 'What happens if a deal doesn’t close or fully fund?',
      answer:
        'In the event that a deal does not close or fully fund, 100% of your investment will be returned. This has not happened one time yet to date.',
    },
    {
      question:
        'How do I keep track of my investment throughout the holding period?',
      answer:
        'We have developed a proprietary investor portal to allow our partners to track their investments and get access to key reports and tax forms. As a partner, you will have your own dedicated account. Formal reports are generated and provided quarterly in addition to monthly leasing updates.',
    },
    {
      question:
        'What reports, updates, and tax forms will I receive throughout the investment hold period?',
      answer:
        'Full financial reports are generated each quarter along with written formal updates. Leasing updates go out monthly. From a tax standpoint, you will receive a K-1 for your interest in the partnership each year. K-1s typically go out by March 15th of each year.',
    },
    {
      question:
        'Do I need to qualify as an accredited investor to invest in your offerings?',
      answer:
        'No. Our offerings are only available under SEC rule 506B to accredited and sophisticated investors.',
    },
    {
      question: 'Can I invest using an IRA or other retirement account?',
      answer:
        'Yes. You will need to utilize the services of a self-directed IRA custodian to facilitate an IRA or other retirement account investment. We can make recommendations to qualified service providers or are happy to work with your custodian in the event you already have engaged with one.',
    },
    {
      question: 'What returns should I expect?',
      answer:
        'Anticipated returns will depend on the risk profile of each transaction and the terms of the offering. All returns presented on the platform are net of all fees. While we model returns conservatively, and do extensive underwriting on each offering before putting it on the platform, it’s important to note that all investments carry risk. Be sure to review the specific set of risk factors for each offering you consider.',
    },
    {
      question:
        'What happens to my investment commitment on a deal that doesn’t make it to full funding?',
      answer:
        'Most offerings hit or exceed their full funding amount. In the event that an offering fails to hit its funding target, all investor commitments will be refunded in full with no fees deducted. Please note that, to date, this situation has never occurred.',
    },
    {
      question: 'How do I transfer funds for an investment?',
      answer:
        'Once you determine you are investing in a particular offering you will be guided through a secure process to complete your investment. Funding can be completed by check or wire.',
    },
    {
      question:
        'What is the minimum investment amount for projects offered on First National Realty Partners?',
      answer:
        'The minimum will vary from offering to offering. The investment minimum is most commonly $75,000. Additional shares are typically offered in increments of $1,000 above the minimum.',
    },
    {
      question:
        'Can non-US citizens invest in offerings in Good Samaritan Capital LLC?',
      answer:
        'Any person or entity with a U.S. tax identification number (social security number or Employer Identification Number), and who meets the SEC’s definition of accredited or sophisticated investor is eligible to participate in Good Samaritan Capital investments. While most of our investors are U.S. citizens, some are legal residents of the United States or foreign nationals who own or partially own an investing entity incorporated in the United States.',
    },
    {
      question: 'What is an accredited investor?',
      answer:
        'For the full definition please click here. The federal securities laws define the term “accredited investor” as any of the following: \
    a natural person with income exceeding $200,000 in each of the two most recent years or joint income with a spouse exceeding $300,000 for those years and a reasonable expectation of the same income level in the current year\
    a natural person who has individual net worth, or joint net worth with the person’s spouse, that exceeds $1 million at the time of the purchase, excluding the value of the primary residence of such person\
    a corporation, partnership or charitable organization with assets exceeding $5 million\
    a trust with assets in excess of $5 million, not formed to acquire the securities offered, whose purchase is directed by a sophisticated person\
    a director, executive officer, or general partner of the company selling the securities\
    a business in which all the equity owners are accredited investors\
    a bank, insurance company, registered investment company, business development company, or small business investment company; or\
    an employee benefit plan, within the meaning of the Employee Retirement Income Security Act, if a bank, insurance company, or registered investment adviser makes the investment decisions, or if the plan has total assets in excess of $5 million.\
',
    },
    {
      question: 'How do I invest with Good Samaritan Capital?',
      answer:
        'Investing with Good Samaritan Capital is designed to be both easy and secure. We’ve created a simple step-by-step process to help you secure your investment in our properties. You can review due-diligence information, ask questions, and if you decide to invest, complete your funding online or by mail.',
    },
  ];

  const faqlist = items.map((faq) => (
    <div className="pt-6 md:grid md:grid-cols-12 md:gap-8">
      <dt className="text-base font-medium text-gray-900 md:col-span-5">
        {faq.question}
      </dt>
      <dd className="mt-2 md:mt-0 md:col-span-7">
        <p className="text-base text-gray-500">{faq.answer}</p>
      </dd>
    </div>
  ));

  return (
    <div className="bg-gray-50">
      <div className="max-w-7xl mx-auto py-12 px-4 divide-y-2 divide-gray-200 sm:px-6 lg:py-16 lg:px-8">
        <h2 className="text-3xl font-extrabold text-gray-900 sm:text-4xl">
          Frequently asked questions
        </h2>
        <div className="mt-6">
          <dl className="space-y-8 divide-y divide-gray-200">{faqlist}</dl>
        </div>
      </div>
    </div>
  );
};
export default FaqSection;
