const MapSection = () => {
  return (
    <div className="text-center mt-16 mx-auto sm:mt-24">
      <h1 className="text-4xl pb-12 tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl">
        <span className="block xl:inline">Apartment</span>
        <span className="block text-indigo-600 xl:inline"> Location</span>
      </h1>
      <iframe
        src="https://www.google.com/maps/d/embed?mid=14cAl3hRqJuf_vPvo-r8jN_kNFFPp5d7y"
        width="100%"
        title="Map of Apartments"
        height="400"
      />
    </div>
  );
};
export default MapSection;
