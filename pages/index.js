import Head from 'next/head';
import Link from 'next/link';

// import AuthProvider from '../authentication/context/auth';
import { useAuth } from '../authentication/context/auth';

export default function IndexPage() {
  const auth = useAuth();
  // const setUser = useAuthUpdate();

  return (
    <div>
      <Head>
        <title>
          Authentication with Amplify, React Hook form and Tailwind CSS
        </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
          <div className="max-w-md w-full space-y-8">
            <div>
              <img
                className="mx-auto h-12 w-auto"
                src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
                alt="Workflow"
              />
              <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
                Real Estate Investing for Professionals
              </h2>
            </div>
            <Link href="/user/login">
              <a className="block text-blue-600 text-2xl">Login</a>
            </Link>
            <Link href="/user/register">
              <a className="block text-blue-600 text-2xl">Register</a>
            </Link>
            <Link href="/user/forgot-password">
              <a className="block text-blue-600 text-2xl">Forgot Password</a>
            </Link>

            <h1>{auth.username()}</h1>

            <span className="relative z-0 inline-flex shadow-sm rounded-md">
              <button
                type="button"
                onClick={() => auth.setUser({ username: 'Daniel Holmlund' })}
                className="relative inline-flex items-center px-4 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
              >
                Daniel
              </button>
              <button
                type="button"
                onClick={() => auth.setUser({ username: 'Matthew Holmlund' })}
                className="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
              >
                Matthew
              </button>
              <button
                type="button"
                onClick={() => auth.setUser({ username: 'Michael Holmlund' })}
                className="-ml-px relative inline-flex items-center px-4 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
              >
                Michael
              </button>
            </span>
          </div>
        </div>
      </main>
    </div>
  );
}
