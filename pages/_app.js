import React from 'react';
import { AuthProvider } from '../authentication/context/auth';
import Navbar from '../src/layout/navbar';

import 'tailwindcss/tailwind.css';

function MyApp({ Component, pageProps }) {
  return (
    <AuthProvider>
      {/* <Navbar /> */}
      <Component {...pageProps} />
    </AuthProvider>
  );
}

export default MyApp;
