import { Parallax } from 'react-parallax';
import Image from 'next/image';

import HeroSection from '../src/sections/hero';
import VideoSection from '../src/sections/video';
import MapSection from '../src/sections/map';
import GallerySection from '../src/sections/gallery';
import FaqSection from '../src/sections/faq';

const PropertyPage = () => {
  let title = 'Willow Park and the Ridges at 5th';
  let propertyaddress = 'Des Moines, Iowa';

  //   let img_style = `background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),
  //        url('${src}'); background-origin: border-box; background-position: center center; background-size: cover; background-attachment: fixed`;

  return (
    <>
      <header
        style={{
          background:
            'linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("/assets/background.jpg")',
          backgroundOrigin: 'border-box',
          backgroundPosition: 'center center',
          backgroundSize: 'cover',
          backgroundAttachment: 'fixed',
        }}
      >
        <HeroSection title={title} address={propertyaddress} />
      </header>
      <VideoSection title="Deal ROOM" />
      <MapSection />

      <GallerySection />
      <FaqSection />
    </>
  );
};
export default PropertyPage;
