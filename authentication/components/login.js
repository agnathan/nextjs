import { useState } from 'react';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { useAuth } from '../context/auth';

export default function SignIn() {
  const { register, handleSubmit } = useForm();
  const [isError, setIsError] = useState(false);
  const auth = useAuth();
  const router = useRouter();

  async function submit({ username, password }) {
    try {
      const ret = await auth.signIn(username, password);

      router.push('/client-protected');
    } catch (error) {
      console.log('error signing in', error);
    }
  }

  const inputStyleTop =
    'appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm ' +
    `${isError ? 'border-red-400 focus:border-red-400' : ''}`;

  const inputStyleButtom =
    'appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm ' +
    `${isError ? 'border-red-400 focus:border-red-400' : ''}`;

  const textStyle = 'text-sm text-red-400' + `${isError ? '' : ' hidden'}`;

  return (
    <form className="mt-8 space-y-6" onSubmit={handleSubmit(submit)}>
      <input type="hidden" name="remember" value="true" />
      <div className="rounded-md shadow-sm -space-y-px">
        <div>
          <label htmlFor="username" className="sr-only">
            Username
          </label>
          <input
            ref={register}
            id="username"
            name="username"
            type="username"
            required
            className={inputStyleTop}
            placeholder="Username"
          />
        </div>
        <div>
          <label htmlFor="password" className="sr-only">
            Password
          </label>
          <input
            ref={register}
            id="password"
            name="password"
            type="password"
            autoComplete="current-password"
            required
            className={inputStyleButtom}
            placeholder="Password"
          />
        </div>
        <div className={textStyle}>
          Wrong password. Try again or click Forgot password to reset it.
        </div>
      </div>

      <div className="flex flex-row items-center justify-end">
        <div className="text-sm">
          <button className="font-medium text-indigo-600 hover:text-indigo-500 mr-4">
            Sign up for an account
          </button>
          <button className="block font-medium text-indigo-600 hover:text-indigo-500">
            Reset Your Password
          </button>
        </div>
      </div>

      <div>
        <button
          type="submit"
          className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          <span className="absolute left-0 inset-y-0 flex items-center pl-3">
            <svg
              className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                clipRule="evenodd"
              />
            </svg>
          </span>
          Sign in
        </button>
        <button
          className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          onClick={() => {
            setIsError(!isError);
          }}
        >
          Toggle Error State
        </button>
      </div>
    </form>
  );
}
