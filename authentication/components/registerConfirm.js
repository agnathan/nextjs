import { useAuth } from '../context/auth';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router';

export default function ConfirmSignup({ user }) {
  const { register, handleSubmit } = useForm();
  const auth = useAuth();
  const router = useRouter();

  async function confirmSignUp({ username, password, code }) {
    try {
      await auth.confirmRegister(username, password, code);

      router.push('/client-protected');
    } catch (error) {
      console.log('error confirming sign up', error);
    }
  }

  return (
    <form className="my-8 " onSubmit={handleSubmit(confirmSignUp)}>
      <input type="hidden" name="remember" value="true" />
      <div>
        <label htmlFor="code" className="sr-only">
          User Name
        </label>
        <input
          ref={register}
          id="username"
          name="username"
          type="text"
          className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
          required
          placeholder="Enter your username"
        />
      </div>

      <div className="rounded-md shadow-sm">
        <div>
          <label htmlFor="code" className="sr-only">
            Code
          </label>
          <input
            ref={register}
            id="code"
            name="code"
            type="number"
            required
            className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
            placeholder="Enter verification code"
          />
        </div>
      </div>

      <div>
        <label htmlFor="code" className="sr-only">
          New Password
        </label>
        <input
          ref={register}
          id="password"
          name="password"
          type="password"
          required
          className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
          placeholder="Enter Your Password"
        />
      </div>

      <div>
        <button
          type="submit"
          className="group relative w-full flex justify-center py-2 px-4 mt-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          <span className="absolute left-0 inset-y-0 flex items-center pl-3">
            <svg
              className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                clipRule="evenodd"
              />
            </svg>
          </span>
          Confirm
        </button>
      </div>
    </form>
  );
}
