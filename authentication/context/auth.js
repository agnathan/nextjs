import { useState, createContext, useContext, useMemo } from 'react';

import Amplify from 'aws-amplify';
import Auth from '@aws-amplify/auth';
import config from '../../aws-exports';

Amplify.configure({
  ...config,
  ssr: true,
});

export const AuthContext = createContext();

export function AuthProvider({ children }) {
  const auth = useProviderAWSAuth();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}

export const useAuth = () => {
  return useContext(AuthContext);
};

function useProviderAWSAuth() {
  const [user, setUser] = useState(null);

  const isAuth = () => {
    if (user === null) {
      console.log('isAuth function null: ', user);
      return false;
    } else {
      console.log('isAuth function not null: ', user);
      return true;
    }
  };

  const register = async (email, username, password) => {
    const ret = await Auth.signUp({
      username,
      password,
      attributes: {
        email, // optional but not in this case as MFA/Verification code wil be emailed
      },
    });

    // AWS returns an object that can be stored
    setUser(ret);
  };

  const confirmRegister = async (username, password, code) => {
    await Auth.confirmSignUp(username, code);
    await Auth.signIn(username, password);
  };

  const username = () => {
    // console.log('authcontext isAuth(): ', isAuth());
    return user != null ? user.username : '';
  };

  const signIn = async (username, password) => {
    const ret = await Auth.signIn(username, password);
    // console.log('login ret: ', ret);
    setUser(ret);
  };

  const signOut = async () => {
    const ret = await Auth.signOut();
    setUser(null);
  };

  const forgotPassword = async (username) => {
    await Auth.forgotPassword(username);
  };

  const forgotPasswordSubmit = async (username, code, password) => {
    await Auth.forgotPasswordSubmit(username, code, password);
  };

  return {
    user,
    setUser,
    isAuth,
    signIn,
    signOut,
    register,
    username,
    confirmRegister,
    forgotPassword,
    forgotPasswordSubmit,
  };
}
